---
layout: page
title: About Us
permalink: /about/
nav: true
---



![](/assets/JulianValerianChina.jpg){: .image-med-large }{: .float-left }
**OpenFlexure Industries was formed to increase access to our open source scientific hardware.**

As researchers developing open source science hardware at the University of Bath we wanted everyone to have access to the instruments we developed. Open source designs only go so far as many people don't have the resources to print their own hardware. OpenFlexure Industries was formed to produce our hardware for those who are unable to produce their own.

![](/assets/RichardIHI.jpg){: .image-med-large }{: .float-right }
**OpenFlexure Industries supports distributed manufacturing.**
 
 With low-cost digital fabrication, provided by 3D printing, it make little sense to send a microscope stage halfway around the world when it can be printed locally. OpenFlexure Industries is interested in supporting other businesses that want to produce OpenFlexure hardware for their local community. Our partners [STICLab](https://sticlab.co.tz/) are producing OpenFlexure microscopes in Tanzania, and the charity [TechForTrade](http://www.techfortrade.org/) produce a modified OpenFlexure microscope from recycled PET bottles for schools in Kenya.
 
  
![](/assets/JulianLondon.jpg){: .image-med-large }{: .float-left }
**OpenFlexure Industries is committed to openness.**

Everything OpenFlexure Industries produces is 100%open source. If you buy a microscope or microscope kit from us you will get the exact same microscope as if you print and assemble it yourself. You are only paying for the materials and our time. Visit [openflexure.org](https://openflexure.org) to learn more about the open source project.

## The team

|![](/assets/Richard.jpg){: .image-medium }| ![](){: .image-medium }|![](/assets/Julian.jpg){: .image-medium }|
|------------------------------| -|---------------------------|
| **Richard Bowman**  |  | **Julian Stirling**  |
| Director and Co-Founder   |  | Director and Co-Founder |

